# Sunrise client
With Sunrise you can easly control your daily plans. With an easy but powerful UI it shows daily forecasts, events, reminders, news and much more!
![Calendar](./images/schedule.svg)

## Functionalities
* Shows the actual time and date, always on top
* Sync securely your calendar to show your events
* Keep track of your Google Tasks memos
* Show the weather based on your location
* Other capabilities coming soon...

## Account
No need for Sunset account. Login via Google using your personal account and see the magic happens! Google Calendar and Google Tasks will synk automatically and you can easely start to use Sunset.

## Security and Privacy
Your data are personal and we don't use them in any way. Google Login is completely based on Google official code and the process is achieved using industry-standard security protocols. Sunset has no remote server and do not store your data remotely, so you can keep your personal data on your device.
To know more about how your data are used, please write to niccolo.segato@icloud.com

## API provider
* Calendar and To-Do APIs use Google technologies.
* News APIs use [] technologies.
* Weather APIs use OpenWeather data.

## License
Sunrise Client is open-source and without ADs
Sunrise Client uses third-party technologies that might have different license type.
Google Calendar, Google Keep, Google logo and other Google technologies are trademark of Google Inc. All rights reserved.
[Go to Google License website](https://support.google.com/a/answer/6309862?hl=en)
OpenWeatherMap data are provided by OpenWeather. [Go to OpenWeather License website](https://openweather.co.uk/privacy-policy)
